# Brikit Jira

Containerized Atlassian Jira on AWS

# Dependencies

yac!

pip install yac

# Use Cases

## Print a Stack

The following command will build the entire Jira stack. The stack includes containers that implement Jira, Postgres and data backups. 

The containers are orchestrated through ECS.

```
yac stack brikit/jira
```

The command will prompt you for required inputs including:

	* the environment you are building for, and
	* the version of Jira you want to run, and
	* the version of Postgres you want to run, and
	* the password for the DB sys admin user

The command will then setup the DB and print instructions needed to complete the site setup.

## Setup the JIRA DB

After the stack print completes, Postgres will be ready to recieve the DB and role used by JIRA. 

This can be setup by running.

```
yac task brikit/jira setupdb
```

The command can be re-run at a later time to 'reset' the Jira DB to an empty state (useful for dev environments).

## Setup the A-Record

JIRA and Postgres run on a single EC2 instance.

Create an A-Record with your domain registrar that aliases a friendly name to the EC2 IP address.

Best practice is to first associate an Elastic IP with your instance, and point your A-Record to the Elastic IP.

This way, you can tear down and rebuild your EC2 instance in a given environment without having to modify the A-Record

## Setup SSL Certificate

JIRA terminates SSL.

To install or renew an SSL certificate, run the following command:

```
yac task brikit/jira keystore
```

The command will prompt you for the location of a pem-formatted cert.

For new stacks, the command will also ask you for the location of pem-formatted root and intermediate certs from you certificate authority.

## Complete the Setup Wizard

After the DB and SSL certs are in place, and your A-Record starts resolving, you can browse to JIRA and complete the setup wizard.  

## Upgrade Jira

Simply re-run the 'yac stack' command used to build the original stack.

When prompted, enter the new version of Jira or Postgres desired.

## Upgrade Postgres

The Postgres container employed is from the official repository in Docker Hub.

See https://hub.docker.com/_/postgres/

The versions available are per the tags in Docker Hub and should be per the 'supported platforms' in the version of JIRA being run.

To upgrade Postgres, simply re-run the 'yac stack' command used to build the original stack.

When prompted, enter the new version of Postgres desired.


## Take a 'snap' backup

To force a backup (aka "snapshot"), run the following:

```
yac task brikit/jira snapshot
```

The command will prompt you for:

	* the environment you are backing up

## Develop the service

Make changes to this repo, and run yac stack against the local Servicefile as ....

```
yac stack <repo path>/Servicefile.json
```