import unittest, os, random
from lib.inputs import save_sites


class TestCase(unittest.TestCase):

    # test that all of the lines gets returned, with a cr between lines
    def test_inputs(self): 
        
        test_parameters = {
            "service-alias" : {
              "value": "inputs-test"
            },
            "env" : {
              "value": "dev"
            },
            "s3-bucket" : {
              "value": "brikit-internal-yac-apps"
            }  
        }

        sites = [ 
            {
                "cname": "kpmg",
                "version": "5.8.18",
                "state": "in_use"
            },
            {
                "cname": "nikecis",
                "version": "5.9.1",
                "state": "in_use"
            }
        ]

        save_sites(test_parameters, sites)

        self.assertTrue(True) 