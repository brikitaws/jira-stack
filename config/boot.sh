#!/bin/bash

# *****************************************************************
# Load and run file used for bootstrapping a new jira host.
# Note that this stuff ONLY gets run at instance creation time or when
# EC2/UserData is updated.
# *****************************************************************

# log stdout and stderr from this script to a boot log
# You can troubleshoot boot issues by inspecting this log file.
logfile=/tmp/boot.log
exec > $logfile 2>&1

# params in curly brackets and CF Refs get rendered in via yac/lib/boot.py

# format volumes as an ext4 fs
FSTYPE=$(blkid -o value -s TYPE /dev/xvdc)

if [ ! $FSTYPE ] ; then
	echo $(date "+%m-%d-%Y %H:%M:%S") "Formatting /dev/xvdc"
	mkfs -t ext4 /dev/xvdc
fi

FSTYPE=$(blkid -o value -s TYPE /dev/xvdd)

if [ ! $FSTYPE ] ; then
	echo $(date "+%m-%d-%Y %H:%M:%S") "Formatting /dev/xvdd"
	mkfs -t ext4 /dev/xvdd
fi

# Create the mount point
if [ ! -d /var/local ]; then
     echo $(date "+%m-%d-%Y %H:%M:%S") "Creating /var/local mount point ..."
     mkdir /var/local
fi

if [ -d /var/local ]; then
     echo $(date "+%m-%d-%Y %H:%M:%S") "Mounting /dev/xvdd as /var/local ..."
	# mount /var/local on /dev/xvdd such that jira home and postgres home are hosted
	# on the xvdd EBS volume
	mount /dev/xvdd /var/local
fi

if [ ! -d /var/lib/docker ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "Creating /var/lib/docker ..."
	mkdir -p /var/lib/docker
fi

if [ -d /var/lib/docker ]; then
     echo $(date "+%m-%d-%Y %H:%M:%S") "Mounting /dev/xvdc as /var/lib/docker ..."
	# mount /var/lib/docker on /dev/xvdc such that all docker images and containers are hosted
	# on the xvdc EBS volume
	mount /dev/xvdc /var/lib/docker
fi

if [ ! -d "/var/local/atlassian" ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "Creating /var/local/atlassian"

	# recursively create the home dir for jira.
	mkdir -p /var/local/atlassian/jira

	# create a directory under home for backups to log to
	mkdir /var/local/atlassian/jira/backup-logs

	# change owner to daemon
	chown -R daemon:daemon /var/local/atlassian/jira

	# give r/w/x(traversal) to everyone under jira home
	# (note: this needs to happen AFTER the chown above)
	chmod 777 -R /var/local/atlassian/jira
fi

# ntp-servers are defined in vpc defs
NTP_SERVERS="{{ntp-servers}}"

# configure NTP
if [ $NTP_SERVERS ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "NTP setup starting ..."

	# break existing symbolic link to shared conf file
	rm /etc/ntp.conf
	# write the servers into a new conf file. servers are in a semicolon delimitted string.
	# sed 'em into a conf file with one line per server
	echo $NTP_SERVERS | sed -e 's/;/\n/g' | sed -e 's/^/server /' > /etc/ntp.conf
	chmod 0666  /etc/ntp.conf
	# restart the ntp daemon so it picks up the new configs
	systemctl restart ntpd

	echo $(date "+%m-%d-%Y %H:%M:%S") "NTP setup complete ..."
fi

# set timezone
timedatectl set-timezone America/Los_Angeles

echo "Remote api setup starting ..."

# create the docker-tcp.socket file, which enables docker's remote API on coreos
echo '[Unit]
	Description=Docker Socket for the API

	[Socket]
	ListenStream=5555
	BindIPv6Only=both
	Service=docker.service

	[Install]
	WantedBy=sockets.target' > /etc/systemd/system/docker-tcp.socket

echo $(date "+%m-%d-%Y %H:%M:%S") "Remote api setup complete ..."

chmod 0644  /etc/systemd/system/docker-tcp.socket

# reload and restart docker to pick up with the new proxy and docker configurations
systemctl enable docker-tcp.socket
systemctl stop docker
systemctl daemon-reload
systemctl start docker-tcp.socket
systemctl start docker

# tell the update engine to reboot on upgrade 
echo "GROUP=stable" > /etc/coreos/update.conf
echo "REBOOT_STRATEGY=reboot" >> /etc/coreos/update.conf

# restart update-engine to pick up configs
systemctl restart update-engine

# tell locksmith to only allow reboots between 1 and 2 am on Sunday
# thus, coreos will only ever upgrade itself during this time period
echo '[Service]
	Environment="REBOOT_WINDOW_START=Sun 01:00"

	Environment="REBOOT_WINDOW_LENGTH=1"' > /run/systemd/system/locksmithd.service.d/20-cloudinit.conf

# restart locksmith to pick up the configs
systemctl restart locksmithd

# ID of the elastic file system that Postgres WAL files get persisted under.
# ID is defined in the servicefile for jira and gets loaded here via a stack parameter
export EFS_ID={"Ref": "AppEFS"}

if [ $EFS_ID ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "EFS setup start ..."

	EFS_MOUNT_DIR=/efs
	
	if [ ! -d $EFS_MOUNT_DIR ]; then
		mkdir $EFS_MOUNT_DIR
	fi
	
	echo $(date "+%m-%d-%Y %H:%M:%S") "Mounting EFS ..."	

	# create the NFS mount

	mount -t nfs4 $(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).$EFS_ID.efs.us-west-2.amazonaws.com:/ $EFS_MOUNT_DIR

	if [ ! -d $EFS_MOUNT_DIR/postgres ]; then

		echo $(date "+%m-%d-%Y %H:%M:%S") "Initializing EFS postgres dirs ..."	

		# make the paths we need under efs
		mkdir -p $EFS_MOUNT_DIR/postgres/archive
		mkdir -p $EFS_MOUNT_DIR/postgres/backups
		mkdir -p $EFS_MOUNT_DIR/postgres/pg_xlog

		# make everything under the mountdir readable/writeable to all
		chmod 777 -R $EFS_MOUNT_DIR
	fi
fi

echo $(date "+%m-%d-%Y %H:%M:%S") "EFS setup complete ..."

# Restore configs tells the backup container what files to restore at instance boot.
# The configs are the same as the backup configs (restore is backup run backwards!)
# Configs are defined in the Servicefile for jira and get loaded here via a stack parameter
RESTORE_CONFIG={"Ref": "RestoreConfigs"}

HOST_NAME={{host-name}}

if [ $RESTORE_CONFIG ]; then

	# If the dbconfig.xml file does not exist assume that Jira is starting on a new,
	# empty, EBS volume, and that we should restore dbconfigs and other files from backups.
	if [ ! -f /var/local/atlassian/jira/dbconfig.xml ]; then

		echo $(date "+%m-%d-%Y %H:%M:%S") "Restoring Jira home from S3 backups ..."

		# restore any home directory files that this instance should have at startup
		# detach so this can run in the background (attachments take a long time to restore
		# and needn't interrupt the boot up process)
		docker run \
		--detach=true \
		--volume=/var/local/atlassian/jira:/var/local/atlassian/jira:rw \
		--volume=/var/local/atlassian/jira/backup-logs:/var/local/backups:rw \
		nordstromsets/backups:latest \
		python -m src.restore $RESTORE_CONFIG $HOST_NAME

		echo $(date "+%m-%d-%Y %H:%M:%S") "Jira home restore complete..."
	fi
fi

PG_HOME={{pg-home}}

if [ ! -d "$PG_HOME/data" ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "Creating $PG_HOME/data"

	mkdir -p $PG_HOME/data
	chmod 777 -R $PG_HOME/data
fi

# pg restore configs provide params for the pg-pitr container.
# They provide paths and other info needed to restore Postgres at EC2 instance boot time.
# Configs are defined in the Servicefile for jira and get loaded here via a stack parameter
export PG_RESTORE_CONFIG={"Ref": "PGRestoreConfigs"}

if [ $PG_RESTORE_CONFIG ]; then

	# Sysadmin user/pwd
	POSTGRES_SYSADMIN="{{postgres-super-user}}"
	POSTGRES_SYSADMIN_PWD="{{postgres-super-pwd}}"

	# verify that DB files are in place. if not, restore from backups
	itemCount=$(ls -A $PG_HOME/data | wc -l | xargs)

	if [ $itemCount == 0 ] ; then

		echo $(date "+%m-%d-%Y %H:%M:%S") "Postgres restore/setup start ..."

		# No DB files are in place.
		# Restore postgres from backuups to the current time
		recovery_pit=$(date "+%m-%d-%Y %H:%M:%S")

		# run the src/restore.sh script in the pg-pitr container to execute the restore.
		docker run -v $PG_HOME:$PG_HOME \
		 -v $EFS_MOUNT_DIR/postgres:$EFS_MOUNT_DIR/postgres \
		 -e CONFIGS_PATH=$PG_RESTORE_CONFIG nordstromsets/pg-pitr:postgres \
		 src/recover.sh "$recovery_pit"

		# verify a DB was created. If no backups exists, the restore above will result
		# in no DB files
		# If no DB files were created, initialize a new Postgres DB instance ...
		# Why initialize a DB? why not instead let the Postgres container do the 
		# init work once it boots up for the first time as an ECS service? 
		# Because of the bind operation that follows (read on...)
		itemCount=$(ls -A $PG_HOME/data | wc -l | xargs)

		if [ $itemCount == 0 ] ; then

			echo $(date "+%m-%d-%Y %H:%M:%S") "Initializing a fresh Postgres instance ..."

			# user 999 maps to the postgres user who should own the data dir
			chown 999:999 $PG_HOME/data

			# store postgres sysadmin pwd in a file for use by initdb
			echo "$POSTGRES_SYSADMIN_PWD" > /tmp/.pwd

			docker run -v /tmp:/tmp -v $PG_HOME:$PG_HOME \
			  -e PGDATA=$PG_HOME/data postgres:9.4 \
			  gosu postgres initdb -U $POSTGRES_SYSADMIN \
			  --pwfile=/tmp/.pwd $PG_HOME/data
		fi

		echo $(date "+%m-%d-%Y %H:%M:%S") "Postgres restore/setup complete ..."
	fi

	# store postgres sysadmin user/pwd in a file for use by pg_basebackup
	echo "*:5432:*:$POSTGRES_SYSADMIN:$POSTGRES_SYSADMIN_PWD" > $PG_HOME/.pgpass

	# change owner to 999, which corresponds to the postgres user which is the daemon user in 
	# the pg-pitr container
	chown 999 $PG_HOME/.pgpass

	# change permission to r/w for owner only. pg_basebackup requires tight permissions
	# on this file (0600 or tigher)
	chmod 0600 $PG_HOME/.pgpass

	if [ -d "$PG_HOME/data" ]; then

		echo $(date "+%m-%d-%Y %H:%M:%S") "Putting Postgres into PITR mode..."

		# setup postgres into PITR mode by writing configuration files under
		# the postgres data dir.
		# run container as user 999 which corresponds to the postgres user in the 
		# postgres container. this will allow the container to write config files under
		# $PG_HOME/data dir
		docker run -v $PG_HOME:$PG_HOME \
		 -v $EFS_MOUNT_DIR/postgres:$EFS_MOUNT_DIR/postgres \
		 -e CONFIGS_PATH=$PG_RESTORE_CONFIG nordstromsets/pg-pitr:postgres \
		 src/setup.sh
	fi

	echo $(date "+%m-%d-%Y %H:%M:%S") "Postgres restore/setup complete..."
fi

# bind the pg_xlog directory to the efs archive dir
if [ -d "$PG_HOME/data/pg_xlog" ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "Binding active WAL files dir ..."

	# if the $EFS_MOUNT_DIR/postgres/pg_xlog is empty, 
    # copy the contents of the existing pg_xlog directory to efs
    # otherwise, leave the $EFS_MOUNT_DIR/postgres/pg_xlog as is, since
    # it probably has the active WALs (presumably created before the previous
    # host was recycled)
	
    # get a count of the items currenty in the pg_xlog folder under efs
	itemCount=$(ls -A $EFS_MOUNT_DIR/postgres/pg_xlog | wc -l | xargs)

	if [ $itemCount == 0 ] ; then
		echo "Copying active wals into EFS"
		cp -r $PG_HOME/data/pg_xlog $EFS_MOUNT_DIR/postgres

		chown -R 999:999 $EFS_MOUNT_DIR/postgres/pg_xlog
	fi

    # bind the share to the pg_xlog dir under the data path s.t. postgres writes active WAL file segments 
    # to efs rather than ebs (since efs is more durable)
	mount --bind $EFS_MOUNT_DIR/postgres/pg_xlog $PG_HOME/data/pg_xlog

	# make sure the $PG_HOME dir is writeable by all. If it isn't pitr will fail because
	# the pg-pitr container will be unable to rename the $PG_HOME/data dir when doing a 
	# recovery
	chmod 0777 $PG_HOME

	echo $(date "+%m-%d-%Y %H:%M:%S") "Active WAL file binds complete..."
fi

# Cluster name are defined in the servicefile for jira and get loaded here via a stack parameter
CLUSTER_NAME={"Ref": "ECS"}

if [ $CLUSTER_NAME ]; then

	echo $(date "+%m-%d-%Y %H:%M:%S") "ECS cluster setup start ..."

	# install ECS agent and register this instance with the CLUSTER_NAME cluster
	docker run --name ecs-agent \
	--detach=true \
	--restart=on-failure:10 \
	--volume=/var/run/docker.sock:/var/run/docker.sock \
	--volume=/var/log/ecs/:/log \
	--volume=/var/lib/ecs/data:/data \
	--volume=/sys/fs/cgroup:/sys/fs/cgroup:ro \
	--volume=/var/run/docker/execdriver/native:/var/lib/docker/execdriver/native:ro \
	--publish=127.0.0.1:51678:51678 \
	--env=ECS_LOGFILE=/log/ecs-agent.log \
	--env=ECS_LOGLEVEL=debug \
	--env=ECS_DATADIR=/data \
	--env=ECS_DOCKER_GRAPHPATH=/run/docker \
	--env=ECS_CLUSTER=$CLUSTER_NAME \
	amazon/amazon-ecs-agent:latest

	# the above will fail if the container already exists
	docker start ecs-agent

	echo $(date "+%m-%d-%Y %H:%M:%S") "ECS cluster setup complete..."
fi
