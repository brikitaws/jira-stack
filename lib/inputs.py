import re, boto3, os, json, jmespath, sys, argparse
from os.path import expanduser
from urlparse import urlparse
from yac.lib.file import get_file_contents
from yac.lib.inputs import get_inputs, get_stack_service_inputs_value, pp_list
from yac.lib.inputs import get_image_versions, string_validation, get_user_inputs
from yac.lib.variables import get_variable, set_variable
from yac.lib.stack import get_stack_name, stack_exists, get_ec2_ips
from yac.lib.template import apply_stemplate
from yac.lib.container.api import get_connection_str
from yac.lib.container.build import build_image, get_image_name, get_image_versions_local
from yac.lib.container.build import download_dockerfile
from yac.lib.state import load_state, save_state, edit_state
from yac.lib.version import is_first_arg_latest, is_a_version, is_same
from yac.lib.cache import get_cache_value, set_cache_value_dt

# populate dynamic params for the jira service
def get_value(params):

    print "gathering inputs via <servicefile>/lib/inputs.py ..."

    # get only the env input first
    get_inputs(params,"service-inputs", ["env"])

    # get the current set of params jira_state_params from s3
    jira_state_params, s3_bucket = load_jira_state(params)

    # save the bucket used back to the state params. This is also the bucket that will
    # be used for backups, etc.
    set_variable(jira_state_params,'s3-bucket', s3_bucket, "The S3 bucket that is used to persiste Jira state and Jira backups.")

    # only prompt user for state param updates for 'yac stack' commands
    yac_stack = sys.argv[0]=='stack'

    # prompt user to see if setpoint changes are needed
    if (yac_stack and 
        jira_state_params and 
        state_change_prompter("Would you like to change the state of the Jira service for this stack (jira version, postgres version, etc.) ? [y,n] >> ")):

        edit_jira_state(params, jira_state_params)

    # add the jira_state_params to the params
    params.update(jira_state_params)

    # get inputs per service-inputs in the Servicefile
    get_inputs(params)

    # set the default catalina options
    set_catalina_opts(params)

    # ensure host name is set
    set_host_name(params)

    # treat any inputs provided by user inside 'get_updates' as contributions to the current state
    # (new inputs may have been provided because one of the servicefiles has added an input)
    jira_state_params.update(get_user_inputs(params))

    # save the jira_state_params
    save_jira_state(params, jira_state_params)

def get_env(params):

    input_key = "service-inputs"
    env_params = {}
    env = {}
    # get input params
    if inputs_key in params and 'inputs' in params[inputs_key]:

        env['env'] = params[inputs_key]['inputs']['env']
        env_params["env-input"] = env

    get_inputs(env_params)

    return get_variable(env_params,"env")

def set_host_name(params):

    # host name is used by the backups container to ensure log entries can be associated with the 
    # source host

    # create a hostname if one isn't already specified
    host_name = get_variable(params,'host-name','')

    if not host_name:

        # construct the host name using the alias and env
        alias = get_variable(params, 'service-alias')
        env = get_variable(params, 'env')
        host_name = '%s-%s'%(alias,env)

        set_variable(params,'host-name', host_name, "name of host")

def set_catalina_opts(params):

    # core options
    CATALINA_ARGS = (
         "-Xms{{jira-heap-size}}\n" +
         "-Xmx{{jira-heap-size}}\n" 
         "-Datlassian.plugins.enable.wait=600\n" +
         "-Duser.timezone={{time-zone}}\n")

    # add core jvm-related variables
    catalina_options = get_catalina_options(CATALINA_ARGS)
    catalina_options = apply_stemplate(catalina_options,params)

    set_variable(params,'catalina-opts', catalina_options, "JVM params via catalina options")

           # get constants associated with yac apps
def get_catalina_options(catalina_args_str):

    # convert carriage returns to spaces
    catalina_options = catalina_args_str.replace('\n',' ')

    return catalina_options


def state_change_prompter(msg):

    validation_failed = True
    change = False

    # accept y, n, or empty string
    options = ['y','n', '']

    while validation_failed:

        input = raw_input(msg)

        # validate the input
        validation_failed = string_validation(input, options, False)

    # change iff 'y' was input
    if (input and input == 'y'):
        change = True

    return change


def build_jira_image(params,version, build_path):

    image_tag = "%s:%s"%("nordstromsets/confluence",version)

    # get stack name
    stack_name = get_stack_name(params)

    # get the ip address of the target host
    target_host_ips = get_ec2_ips( stack_name , "", True)

    user_msg =  "%sThe %s container image does not yet exist on the %s stack%s"%('\033[92m',
                                            image_tag,
                                            stack_name,                                                    
                                            '\033[0m')

    raw_input(user_msg + "\nHit <enter> to build image ..." )

    # build the container image on each host ...
    for target_host_ip in target_host_ips:

        # get connection string for the docker remote api on the target host
        docker_api_conn_str = get_connection_str( target_host_ip )

        # build the image
        build_image( image_tag, build_path, docker_api_conn_str )

def edit_jira_state(params,jira_state_params):

    # path is the env
    env = get_variable(params,'env',"")
    service_alias = get_variable(params,'service-alias')

    edit_state(env, service_alias, jira_state_params)

def load_jira_state(params):

    # path is the env
    env = get_variable(params,'env',"")
    service_alias = get_variable(params,'service-alias')

    jira_state,s3_bucket = load_state(env,service_alias)

    return jira_state, s3_bucket

def save_jira_state(params, state_params):

    # path is the env
    env = get_variable(params,'env',"")
    service_alias = get_variable(params,'service-alias')

    save_state(env, service_alias, state_params)

def pp_site_list(list):

    str = ""
    for item in list:
        str = str + '* %s@%s\n'%(item['cname'],item['version'])

    return str